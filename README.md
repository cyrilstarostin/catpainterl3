# CatPainterL3

This project demostrates the core OOP principles: Abstraction, Polymorphism,
Incapsulation and Inheritance.

The system under development is software for automatic cat painting (yep, 
you got it right).