package com.oop.painters;


import com.oop.model.Cat;
import com.oop.model.Paint;

import java.awt.*;


public class CatAcrylicPainter extends CatPainter {

    private static final int PAINT_STEP = 1;

    public void paint(Cat cat, Color desiredColor) {
        while (hasPaintLeft(PAINT_STEP) && !isDesiredColorReached(cat.color, desiredColor)) {
            updateCatColor(cat);
            paintStock.paintVolume -= PAINT_STEP;
        }
    }

    private boolean isDesiredColorReached(Color currentColor, Color expectedColor){
        return currentColor.equals(expectedColor);
    }

    private void updateCatColor(Cat cat){
        int newRed = cat.color.getRed() + PAINT_STEP;
        int newGreen = cat.color.getGreen() + PAINT_STEP;
        int newBlue = cat.color.getBlue() + PAINT_STEP;
        cat.color = new Color(newRed, newGreen, newBlue);
    }

    public void refillPaint(Paint capsule) {
        paintStock.paintVolume += capsule.paintVolume;
    }
}
