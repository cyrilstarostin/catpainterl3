package com.oop.painters;

import com.oop.Food;
import com.oop.FoodSource;
import com.oop.model.ButterPaint;
import com.oop.model.Cat;
import com.oop.model.Paint;

import java.awt.*;


public class CatButterPainter extends CatPainter implements FoodSource {
    private static final int MAX_FAT_PAINT_STEP = 10;

    public void paint(Cat cat, Color desiredColor) {
        int paintStep = getPaintStep();
        while (hasPaintLeft(paintStep) && !isEnoughYellow(cat.color, desiredColor)) {
            makeCatColorMoreYellow(cat, paintStep);
            paintStock.paintVolume -= paintStep;
        }
    }

    private int getPaintStep(){
        float fatPercent = ((ButterPaint) paintStock).fatPercent;
        return (int) (MAX_FAT_PAINT_STEP *  fatPercent);
    }

    private boolean isEnoughYellow(Color currentColor, Color expectedColor) {
        boolean isEnoughRed = currentColor.getRed() >= expectedColor.getRed();
        boolean isEnoughGreen = currentColor.getGreen() >= expectedColor.getGreen();
        return isEnoughRed && isEnoughGreen;
    }

    private void makeCatColorMoreYellow(Cat cat, int paintStep) {
        int newRed = cat.color.getRed() + paintStep * paintStock.color.getRed();
        int newGreen = cat.color.getGreen() + paintStep * paintStock.color.getGreen();
        cat.color = new Color(newRed, newGreen, cat.color.getBlue(), 0);
    }

    public void refillPaint(Paint paint) {
        paintStock = paint;
    }

    public Food getFood() {
        return (ButterPaint) paintStock;
    }
}
