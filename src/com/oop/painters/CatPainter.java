package com.oop.painters;

import com.oop.model.Cat;
import com.oop.model.Paint;
import com.oop.model.WaterBucket;

import java.awt.*;


public abstract class CatPainter {

    private static final float MAX_WATER_VOLUME = 1000f;

    protected Paint paintStock;
    private float waterStock;

    public abstract void paint(Cat cat, Color desiredColor);

    public abstract void refillPaint(Paint capsule);

    public void refillWater(WaterBucket bucket) {
        openWaterContainer();
        pourWater(bucket.waterVolume);
        closeWaterContainer();
    }

    private void openWaterContainer() {
        System.out.println("Opened water container");
    }

     private void pourWater(float waterVolume) {
        this.waterStock += waterVolume;
        if (waterStock > MAX_WATER_VOLUME)
            waterStock = MAX_WATER_VOLUME;
    }

     private void closeWaterContainer() {
        System.out.println("Closed water container");
    }

    protected boolean hasPaintLeft(int paintStep){
        return Math.abs(paintStock.paintVolume - paintStep) > 0.0001;
    }
}
