package com.oop;

public interface FoodSource {

    Food getFood();
}
