package com.oop.model;

import java.awt.*;

public class Cat {
    public int id;
    public String name;
    public Breed breed;
    public boolean isAllergic;
    public Color color;


    public Cat(int id, String name, Breed breed, boolean isAllergic, Color color) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.isAllergic = isAllergic;
        this.color = color;
    }
}
