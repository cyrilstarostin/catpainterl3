package com.oop.model;

public enum Breed {
    UNKNOWN,
    SIAMESE,
    AMERICAN,
    JAPANESE
}
