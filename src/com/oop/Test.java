package com.oop;


import com.oop.model.Breed;
import com.oop.model.Cat;
import com.oop.painters.CatAcrylicPainter;
import com.oop.painters.CatButterPainter;
import com.oop.painters.CatPainter;

import java.awt.*;

class Test {

    public static void main(String[] args) {
        Cat cat1 = new Cat(0, "JohnyBoy", Breed.UNKNOWN, true, Color.WHITE);
        Cat cat2 = new Cat(0, "Kitty", Breed.SIAMESE, false, Color.BLACK);
        Cat cat3 = new Cat(0, "Vasilisa", Breed.AMERICAN, true, Color.BLUE);
        Cat cat4 = new Cat(0, "Boris", Breed.JAPANESE, false, Color.CYAN);
    }
}
